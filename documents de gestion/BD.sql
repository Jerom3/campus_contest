DROP TABLE IF EXISTS user ;
CREATE TABLE user (id_users_visiteur BIGINT AUTO_INCREMENT NOT NULL,
first_name_user VARCHAR(50),
last_name_user VARCHAR(50),
date_messages TIMESTAMP,
email_messages VARCHAR,
subject_messages VARCHAR,
PRIMARY KEY (id_users_visiteur)) ENGINE=InnoDB;

DROP TABLE IF EXISTS site ;
CREATE TABLE site (id_site_projet BIGINT AUTO_INCREMENT NOT NULL,
accueil_site VARCHAR,
galerie_site VARCHAR,
contact_site VARCHAR,
PRIMARY KEY (id_site_projet)) ENGINE=InnoDB;

DROP TABLE IF EXISTS formulaire ;
CREATE TABLE formulaire (id_message_formulaire VARCHAR AUTO_INCREMENT NOT NULL,
fname_formulaire VARCHAR,
lname_formulaire VARCHAR,
email_formulaire VARCHAR,
subject_formulaire VARCHAR,
date_formulaire VARCHAR,
PRIMARY KEY (id_message_formulaire)) ENGINE=InnoDB;

DROP TABLE IF EXISTS admin ;
CREATE TABLE admin (id_admin_admin VARCHAR AUTO_INCREMENT NOT NULL,
login_admin BIGINT,
password_admin BIGINT,
PRIMARY KEY (id_admin_admin)) ENGINE=InnoDB;

DROP TABLE IF EXISTS visiter ;
CREATE TABLE visiter (id_users_visiteur **NOT FOUND** AUTO_INCREMENT NOT NULL,
id_site_projet **NOT FOUND** NOT NULL,
PRIMARY KEY (id_users_visiteur,
 id_site_projet)) ENGINE=InnoDB;

DROP TABLE IF EXISTS administrer ;
CREATE TABLE administrer (id_admin_admin **NOT FOUND** AUTO_INCREMENT NOT NULL,
id_site_projet **NOT FOUND** NOT NULL,
id_message_formulaire **NOT FOUND** NOT NULL,
PRIMARY KEY (id_admin_admin,
 id_site_projet,
 id_message_formulaire)) ENGINE=InnoDB;

ALTER TABLE visiter ADD CONSTRAINT FK_visiter_id_users_visiteur FOREIGN KEY (id_users_visiteur) REFERENCES user (id_users_visiteur);

ALTER TABLE visiter ADD CONSTRAINT FK_visiter_id_site_projet FOREIGN KEY (id_site_projet) REFERENCES site (id_site_projet);
ALTER TABLE administrer ADD CONSTRAINT FK_administrer_id_admin_admin FOREIGN KEY (id_admin_admin) REFERENCES admin (id_admin_admin);
ALTER TABLE administrer ADD CONSTRAINT FK_administrer_id_site_projet FOREIGN KEY (id_site_projet) REFERENCES site (id_site_projet);
ALTER TABLE administrer ADD CONSTRAINT FK_administrer_id_message_formulaire FOREIGN KEY (id_message_formulaire) REFERENCES formulaire (id_message_formulaire);
