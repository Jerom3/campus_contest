
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf8">
    <title>Jérôme Chevert - Contact</title>
    <link rel="stylesheet" href="styles.css" />
    <link rel="shortcut icon" type="image/x-icon" href="images/ico_CJ.ico" />
    <link href="https://fonts.googleapis.com/css?family=Righteous&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Lobster|Oxygen|PT+Serif|Satisfy&display=swap" rel="stylesheet"> 
    
</head>

<body>

    <header class="header">

        <div id="name">Jérôme Chevert // Developpeur</div>

        <ul class="menu">
            <li>
                <a href="accueil.html">Bonjour</a>
            </li>
            <li>
                <a href="portfolio.html">Portfolio</a>
            </li>
            <li>
                <a href="contact.php">Contact</a>
            </li>
        </ul>

    </header>


    <main>
        <div class=backstars>
            <div class="containerform" methode="POST">
                <form action="cible.php">
                    <center><a href="mailto:chevert.jerome@orange.fr" style="color:whitesmoke; font-family: 'Lobster', cursive; font-size: 23px; text-decoration:none;"> chevert.jerome@orange.fr </chevert.></a></center><br>
                    <input type="text" id="fname" name="firstname" placeholder="Votre nom">
                    <input type="text" id="email" pattern=".+@globex.com" size="*10" required placeholder="Votre adresse mail">
                    </select>
                    <textarea id="subject" name="subject" placeholder="Votre message" style="height:360px"></textarea>
                    <center><input type="submit" value="Envoyer"></center>
                </form>
            </div> 
        </div>
    </main>


    <footer>
        
        <div class="container2">
            <div class="linked">
                <a href="https://www.linkedin.com/in/jérôme-chevert-a340b4139" target="_blank"><img src="images/linked.png" height="40px" width="40px">
            </div>
        </div>

    </footer>


</body>
</html>